/**
 * The MIT License (MIT)
 * Copyright (c) 2020 铭软科技(mingsoft.net)
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package net.mingsoft.people.biz;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import net.mingsoft.base.biz.IBaseBiz;
import net.mingsoft.people.entity.PeopleEntity;


/**
 * 
 * 用户业务层，继承IBaseBiz
 * @author 铭飞开发团队
 * @version 
 * 版本号：0.0<br/>
 * 创建日期：2017-8-23 10:10:22<br/>
 * 历史修订：<br/>
 */
public interface IPeopleBiz  extends IBaseBiz{

	/**
	 * 根据用户ID删除用户实体，用于有子类的删除操作
	 * @param id 用户ID
	 */
	public void deletePeople(int id);	
	
	
	/**
	 * 批量删除用户
	 * @param peopleIds 用户id集合
	 */
	public void deletePeople(int[] peopleIds);	
	
	/**
	 * 更具用户实体获取用户信息,
	 * @param people 用户实体，可以设置用户名、用户邮箱、用户手机号
	 * @return 用户实体
	 */
	PeopleEntity getByPeople(PeopleEntity people);
	

	/**
	 * 根据用户名(帐号,手机,邮箱)和验证码查询用户信息开始
	 * @param userName 用户名
	 * @param peopleCode 验证码
	 * @return 用户实体
	 */
	public PeopleEntity getEntityByCode(String userName,String peopleCode);
	
	/**
	 * 根据用户用户名查询用户实体</br>
	 * @param userName 用户名(注:手机号,邮箱)
	 * @return 查询到的用户实体
	 */
	PeopleEntity getEntityByMailOrPhone(String userName);
	
	/**
	 * 根据用户用户名查询用户实体</br>
	 * @param userName 用户名(注:手机号,邮箱,用户名称都可作为用户名登录)
	 * @return 查询到的用户实体
	 */
	public PeopleEntity getEntityByUserName(String userName);
	

	/**
	 * 用户有子类添加
	 * @param people 用户实体
	 * @return 用户ID
	 */
	public int savePeople(PeopleEntity people);
	
	/**
	 * 根据用户ID进行用户实体的更新，用于有子类的更新操作
	 * @param people 用户实体
	 */
	public void updatePeople(PeopleEntity people);
	
}
