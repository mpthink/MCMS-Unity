package net.mingsoft.msjars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsJarsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsJarsApplication.class, args);
    }

}
