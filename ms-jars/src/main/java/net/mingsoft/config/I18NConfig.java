package net.mingsoft.config;

import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * @author mpthink
 * @date 2021/3/24 15:55
 */
@Configuration
public class I18NConfig {
    @Autowired
    protected freemarker.template.Configuration configuration;
    @Value("${ms.local.default:zh_CN}")
    private String defaultLocal;
    @Value("$ms.local.language:zh_CN,en_US}")
    private String localLanguage;

    public I18NConfig() {
    }

    @PostConstruct
    public void init() throws IOException, TemplateException {
        this.configuration.setSharedVariable("localDefault", this.defaultLocal);
        this.configuration.setSharedVariable("localLanguage", this.localLanguage.split(","));
    }
}
