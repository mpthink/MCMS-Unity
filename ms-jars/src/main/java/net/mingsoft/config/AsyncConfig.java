package net.mingsoft.config;

/**
 * @author mpthink
 * @date 2021/3/24 15:54
 */
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfig {
    public AsyncConfig() {
    }

    @Bean
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("ms-async");
        executor.setCorePoolSize(3);
        executor.setMaxPoolSize(10);
        executor.initialize();
        return executor;
    }
}