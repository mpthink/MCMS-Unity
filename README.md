# MCMS-Unity

#### 介绍
MCMS 的单项目版本，不会去拉取远程仓库的mcms jar包，方便本地修改和学习。

SQL文件是5.2版本的，MCMS之前的5.2 sql是有问题的，有几个表的alter没有执行

jar版本统一修改为： 1.0-SNAPSHOT

ms-jars包含： ms-base，ms-core,ms-basic,ms-mdiy,ms-mpeople,store-client

swagger访问地址（不要写项目包名，否则访问不了的）：
http://localhost:8080/swagger-ui.html#/

仅供学习

其他请参考：
https://gitee.com/mingSoft/MCMS

